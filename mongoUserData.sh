#!/bin/bash
set -x

TOOLSDIR="/opt/Cloudwords/tools"
BASEMONGOLOGDIR="/var/log/mongo"
DBPATH="/var/lib/mongo/mongo-data"
mkdir -p $DBPATH $BASEMONGOLOGDIR

echo "$TOOLSDIR/mongo/mongodb-linux-x86_64-2.6.8/bin/mongod --dbpath $DBPATH --logpath $BASEMONGOLOGDIR/mongo.log --fork" >> /etc/rc.local
$TOOLSDIR/mongo/mongodb-linux-x86_64-2.6.8/bin/mongod --dbpath $DBPATH --logpath $BASEMONGOLOGDIR/mongo.log --fork
