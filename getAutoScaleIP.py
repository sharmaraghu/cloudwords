import boto
from sys import argv
groupname = argv[1]
autoscaleconn = boto.connect_autoscale()
group = autoscaleconn.get_all_groups([groupname])[0]
instance_id = group.instances[0].instance_id
ec2 = boto.connect_ec2()
res = ec2.get_all_instances(instance_id)

instance_privateIP = res[0].instances[0].private_ip_address

print instance_privateIP
